module ActiveRecord
  module ConnectionAdapters
    class SQLServerAdapter < AbstractAdapter
      def configure_connection
        raw_connection_do "SET ANSI_NULLS ON"
      end
    end
  end
end
